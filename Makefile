# This file is part of racket-maints.

# racket-maints is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.

# racket-maints is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with racket-maints.  If not, see <https://www.gnu.org/licenses/>.

# Original author: Maciej Barć <xgqt@riseup.net>
# Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License
# SPDX-License-Identifier: GPL-3.0-only


MAKE := make

PYTHON-SETUP-OPTS :=
RACO-INSTALL-OPTS := --auto --batch --no-cache --skip-installed


all: install


python-setup:
	cd ./src/python && python3 ./setup.py $(PYTHON-SETUP-OPTS)

python-build:
	$(MAKE) PYTHON-SETUP-OPTS="build" python-setup

python-clean:
	find ./src/python/ -type d -name "__pycache__" -exec rm -dr {} +
	rm -dfr ./src/python/{build,dist,racket_maints.egg-info}


racket-install:
	cd ./src/racket && raco pkg install $(RACO-INSTALL-OPTS) --name racket-maints

racket-remove:
	raco pkg remove --no-trash racket-maints


install:
	$(MAKE) PYTHON-SETUP-OPTS="install $(PYTHON-SETUP-OPTS)" python-setup
	$(MAKE) racket-install
	cd ./src/shell && $(MAKE) install

install-user:
	$(MAKE) DESTDIR=${HOME}/.local PREFIX= PYTHON-SETUP-OPTS="--user" install
