#!/usr/bin/env python3


# This file is part of racket-maints.

# racket-maints is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.

# racket-maints is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with racket-maints.  If not, see <https://www.gnu.org/licenses/>.

# Original author: Maciej Barć <xgqt@riseup.net>
# Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License
# SPDX-License-Identifier: GPL-3.0-only


def get_current_racket_version(vartree, cp):
    m = vartree.dbapi.match(cp)
    return ("" if m == [] else m[-1])


def get_best_racket_version(porttree, cp):
    m = porttree.dbapi.xmatch("bestmatch-visible", cp)
    if m == "":
        raise SystemExit(f"No {cp} found in Portage tree.")
    else:
        return m


def main():

    from racket_maints.lib.private.cli_base import CLI_Base
    c = CLI_Base("Update Racket packages and rebuild packages")

    import portage
    from racket_maints.lib.private import vartree_query as vq
    vartree = portage.db[portage.root]["vartree"]
    porttree = portage.db[portage.root]["porttree"]

    racket_cpv_current = get_current_racket_version(vartree, c.cp_main)
    racket_cpv_best = get_best_racket_version(porttree, c.cp_main)
    if racket_cpv_current == racket_cpv_best:
        print("No Racket update available, exiting.")
        exit(0)

    packages_list = list(filter(
        (lambda cpv: vq.check_dependent(vartree, c.cp_main, cpv)),
        vartree.getallcpv()
    ))
    packages_list += [racket_cpv_best]

    emerge_options = " ".join(["--deep", "--keep-going",
                               "--oneshot", "--update"]
                              + c.extra_options)
    emerge_packages = " ".join(list(map((lambda s: "=" + s), packages_list)))
    emerge_command = f"emerge {emerge_options} {emerge_packages}"

    import os
    print(f"Running: {emerge_command}")
    ret = os.system(emerge_command)
    exit(0) if ret == 0 else exit(1)


if __name__ == "__main__":
    main()
