#!/usr/bin/env python3


# This file is part of racket-maints.

# racket-maints is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.

# racket-maints is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with racket-maints.  If not, see <https://www.gnu.org/licenses/>.

# Original author: Maciej Barć <xgqt@riseup.net>
# Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License
# SPDX-License-Identifier: GPL-3.0-only


def check_property(vartree, cpv, prop):
    """Check if cpv has given property."""
    return (prop in vartree.dbapi.aux_get(cpv, ["PROPERTIES"]))


def check_dependent(vartree, cp_main, cpv):
    """Check if cpv depends on cp_main."""
    lst = list(filter((lambda dep: cp_main in dep),
                      vartree.dbapi.aux_get(cpv, ["DEPEND"])))
    return (lst != [])


def get_all_with_proprieties_live(vartree):
    """Get a list of all packages with "live" property in vartree."""
    f = filter((lambda cpv: check_property(vartree, cpv, "live")),
               vartree.getallcpv())
    return list(f)
