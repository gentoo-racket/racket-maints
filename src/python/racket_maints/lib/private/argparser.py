#!/usr/bin/env python3


# This file is part of racket-maints.

# racket-maints is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.

# racket-maints is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with racket-maints.  If not, see <https://www.gnu.org/licenses/>.

# Original author: Maciej Barć <xgqt@riseup.net>
# Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License
# SPDX-License-Identifier: GPL-3.0-only


import argparse

from racket_maints import __version__


def make_argparser(desc):
    parser = argparse.ArgumentParser(
        description=desc,
        epilog="""Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>,
        licensed under the GNU GPL v3 License"""
    )
    parser.add_argument(
        "-m", "--main-package",
        type=str, default="dev-scheme/racket",
        help="Racket package name"
    )
    parser.add_argument(
        "-e", "--extra-emerge-options",
        type=str, default="",
        help="Extra arguments to add to the emerge command invocation"
    )
    parser.add_argument(
        "-V", "--version",
        action="version",
        version=f"%(prog)s {__version__}"
    )
    return parser
