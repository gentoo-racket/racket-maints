#!/usr/bin/env python3


# This file is part of racket-maints.

# racket-maints is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.

# racket-maints is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with racket-maints.  If not, see <https://www.gnu.org/licenses/>.

# Original author: Maciej Barć <xgqt@riseup.net>
# Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License
# SPDX-License-Identifier: GPL-3.0-only


from setuptools import setup

try:
    from racket_maints import __version__
except ImportError:
    __version__ = "unknown"


setup(
    name="racket_maints",
    version=__version__,
    description="Racket Maintenance Scripts",
    author="Maciej Barć",
    author_email="xgqt@riseup.net",
    url="https://gitlab.com/gentoo-racket/racket-maints",
    license="GPL-3",
    keywords="gentoo portage racket",
    packages=["racket_maints", "racket_maints.lib.private"],
    include_package_data=True,
    zip_safe=False,
    entry_points={"console_scripts": [
        "racket-maints-live-rebuild = racket_maints.live_rebuild:main",
        "racket-maints-recompile-all = racket_maints.recompile_all:main",
        "racket-maints-update-racket = racket_maints.update_racket:main"
    ]},
)
